let user = window.localStorage.user;

let showNuevaPunLi = () => {
    let htmlNuevaPub = "<li class='nav-item' id='li-nueva-pub'>"+
                            "<a class='nav-link' style='cursor: pointer;' id='a-nueva-pub'>"+
                                "<i class='fa fa-pencil-alt'></i> Nueva publicación"+
                            "</a>"+
                        "</li>"
    $("#navbarCollapse .navbar-principal").append(htmlNuevaPub);
}

let $user;
if(typeof user != "undefined"){
    console.log("Entra aquí")
    showNuevaPunLi();
    $user = JSON.parse(user);
    $("#autorPost").val($user.nombre);
    $("#span-user").text($user.nombre);
    $("#nav-user-action #span-user-action").text("Cerrar sesión");
    $("#nav-user-action").show();
    $("#loginCommentDiv").hide();
}else{
    $("#nav-user-action #span-user-action").text("Iniciar sesión");
    $("#nav-user-action").show();
    $("#nav-user-label").hide();
    $(".modal-body-new-comment").hide();
}

$(document).ready(($) => {
    console.log("JS Blog");
    moment.locale('es');
    let divPost = "<div class='col-4 div-post'>"+
                        "<div class='card' style='width: 18rem;'>"+
                        "<img src='' class='card-img-top'>"+
                        "<div class='card-body'>"+
                            "<h5 class='card-title'><span class='titulo'></span> <span class='autor'style='color: #C00'></span></h5>"+
                            "<p class='card-text'></p>"+
                            "<p class='card-text-date'></p>"+
                            "<a href='#' class='btn btn-danger btn-pub-detail'>Comentarios</a>"+
                            "<button type='button' class='btn btn-danger btn-pub-edit' style='margin-left: 10px;'>Editar</button>"+
                        "</div>"+
                    "</div>"+
                "</div>";
    
    let divComment = "<div class='form-group'>"+
                        "<label for='autorComment' class='autorComment'></label>"+
                        "<div class='form-control' id='commentPost'>"+
                            "<span class='commentPostText'></span>"+
                        "</div>"+
                    "</div>";
            
    $("#a-nueva-pub").click(() =>{
        $("#modal-nueva-pub").modal("show");
        $("#tituloPost").val("");
        $("#descripcionPost").val("");
        $("#nueva-pub-img-cont").attr("src","");
    });
    $('.wrapper-login').removeClass('loading').addClass('loaded');
    $("#img-nueva-pub").on("change", function(){
        previewFile("nueva");
    });

    $("#img-edit-pub").on("change", function(){
        previewFile("edit");
    });

    function previewFile($selector) {
        const preview = document.getElementById($selector+'-pub-img-cont');
        const file = document.getElementById('img-'+$selector+'-pub').files[0];

        var ext = $('#img-'+$selector+'-pub').val().split('.').pop().toLowerCase();
        if($.inArray(ext, ['png','jpg']) == -1) {
            alert('Extensión de archivo inválida, solo JPG o PNG.');
            return;
        }

        const reader = new FileReader();
        
        if(typeof file == "undefined"){
            preview.src = "";
            return;
        }
        reader.addEventListener("load", function () {
          // convert image file to base64 string
          preview.src = reader.result;
        }, false);
      
        if (file) {
          reader.readAsDataURL(file);
        }
    }

    $("#btn-send-pub").on("click", function savePublication($post) {

        let $tituloPost = $("#tituloPost");
        let $descripcion = $("#descripcionPost");

        if($tituloPost.val().length < 4){
            alert("Título inválido, mínimo 5 caracteres.");
            return;
        }

        if($descripcion.val().length < 4){
            alert("Descripción muy corta, mínimo 5 caracteres.");
            return;
        }
        
        $post = {
            "idUsuario":$user.idUsuario,
            "titulo": $tituloPost.val(),
            "descripcion": $descripcion.val(),
            "media": $("#nueva-pub-img-cont").attr("src") != "" ? $("#nueva-pub-img-cont").attr("src") : ""
        };
        let savePostPromise = new Promise( async (resolve, reject) => {
            $.ajax({
                method: "POST",
                url: "/posts",
                dataType: "json",
                data: $post,
                success: function($data) {
                    return $data;
                },
                error: function(qXHR, textStatus, errorThrown){
                    alert("Error al dar de alta "+textStatus);
                    return null;
                }
            })
            .done(function( data ) {
                if(data != null){
                    let post = $(divPost);//Creamos el contenedor para el post
                    post.find(".card-img-top").attr("src",$post.media);
                    post.find(".titulo").text($post.titulo+" por ");
                    post.find('.autor').text($user.nombre);
                    post.find(".card-text").text($post.descripcion);
                    post.find(".card-text-date").text(moment().format("llll"));
                    post.find(".btn-pub-detail").attr("data-idpost",data);
                    post.find(".btn-pub-edit").attr("data-idpost",data);
                    $("#div-posts").prepend(post);
                    console.log( "Data: ", data );
                    $tituloPost.val("");
                    $descripcion.val("");
                    $("#nueva-pub-img-cont").attr("src","");
                    $("#modal-nueva-pub").modal("hide");
                    resolve(data);
                }
            }).fail((qXHR, textStatus, errorThrown) => {
                reject("Error "+textStatus);
            });
        });

        savePostPromise.then((data) =>{
            console.log("Save post ",data);
        });
    });

    $("#btn-edit-pub").on("click", () => {
        let $idPost = $("#modal-editar-pub").attr("data-idpost");

        let $tituloPost = $("#tituloPostEdit");
        let $descripcion = $("#descripcionPostEdit");

        if(typeof $idPost == "undefined"){
            alert("Post no válido.");
            return;
        }

        if($tituloPost.val().length < 4){
            alert("Título inválido, mínimo 5 caracteres.");
            return;
        }

        if($descripcion.val().length < 4){
            alert("Descripción muy corta, mínimo 5 caracteres.");
            return;
        }
        
        $post = {
            "idUsuario":$user.idUsuario,
            "idPost": $idPost,
            "titulo": $tituloPost.val(),
            "descripcion": $descripcion.val(),
            "media": $("#edit-pub-img-cont").attr("src") != "" ? $("#edit-pub-img-cont").attr("src") : ""
        };
        let savePutPromise = new Promise( async (resolve, reject) => {
            $.ajax({
                method: "PUT",
                url: "/posts",
                dataType: "json",
                data: $post,
                success: function($data) {
                    return $data;
                },
                error: function(qXHR, textStatus, errorThrown){
                    alert("Error al dar de alta "+textStatus);
                    return null;
                }
            })
            .done(function( data ) {
                if(data != null){
                    let post;
                    $.each($(".div-post"), function(i,v){
                        let $idPostAux = $(v).find(".btn-pub-detail").attr("data-idpost");
                        if($idPostAux == $idPost){
                            post = $(v);
                        }
                        
                    });
                    
                    post.find(".card-img-top").attr("src",$post.media);
                    post.find(".titulo").text($post.titulo+" por ");
                    post.find(".card-text").text($post.descripcion);
                    console.log( "Data: ", data );
                    $tituloPost.val("");
                    $descripcion.val("");
                    $("#modal-editar-pub").modal("hide");
                    resolve(data);
                }
            }).fail((qXHR, textStatus, errorThrown) => {
                reject("Error "+textStatus);
            });
        });

        savePutPromise.then((data) =>{
            console.log("Save post ",data);
        });
    });

    $(document).on("click",".nav-user-action",() => {
        if(typeof $user != "undefined"){
            window.localStorage.removeItem("user");
            window.location.href = "/";
        }else{
            $(".modal").modal("hide");
            $("#modal-login").modal("show");
        }
    });

    $(".nav-user-register").on("click", () => {
        $(".modal").modal("hide");
        $("#modal-register").modal("show");
    });

    $("#modal-register").on("hidden.bs.modal", () => {

    });

    $(document).on("click",".btn-pub-edit", (e) =>{
        console.log("Element ",$(e.target));
        let $idPost = $(e.target).attr("data-idpost");
        console.log("Id post ",$idPost);
        let $tituloPost = $("#tituloPostEdit");
        let $descripcion = $("#descripcionPostEdit");
        let $autor = $("#autorPostEdit");
        if(typeof $idPost == "undefined"){
            //alert("Post no válido.");
            return;
        }

        $("#modal-editar-pub").attr("data-idpost",$idPost);
        $tituloPost.val("");
        $descripcion.val("");
        let getPostDetailPromise = new Promise( async (resolve, reject) => {
            $.ajax({
                method: "GET",
                url: "/posts/"+$idPost+"/"+$user.idUsuario,
                dataType: "json",
                success: function($data) {
                    return $data;
                },
                error: function($data){
                    alert("No se pudo recuperar la información.");
                    reject(null);
                }
            })
            .done(function( data ) {
                resolve(data);
            });

        });
        getPostDetailPromise.then((data) =>{
            console.log("Return promise comments ",data);
            if(data != null){
                $tituloPost.val(data.titulo);
                $descripcion.val(data.descripcion);
                $autor.val($user.nombre);
                $("#edit-pub-img-cont").attr("src",data.media);
                $("#modal-editar-pub").modal("show");
            }
        });
    });

    $("#btn-edit-delete-pub").on("click", () => {
        let $idPost = $("#modal-editar-pub").attr("data-idpost");
        if(typeof $idPost == "undefined"){
            alert("No se puede eliminar el post.");
            return;
        }

        let getDeleteDetailPromise = new Promise( async (resolve, reject) => {
            $.ajax({
                method: "DELETE",
                url: "/posts/"+$idPost+"/"+$user.idUsuario,
                dataType: "json",
                success: function($data) {
                    return $data;
                },
                error: function($data){
                    alert("Hubo un error al eliminar el post, intente nuevamente más tarde.");
                    reject(null);
                }
            })
            .done(function( data ) {
                resolve(data);
            });

        });
        getDeleteDetailPromise.then((data) =>{
            console.log("Return promise comments ",data);
            if(data != null){
                $.each($(".div-post"), function(i,v){
                    let $idPostAux = $(v).find(".btn-pub-detail").attr("data-idpost");
                    
                    if($idPostAux == $idPost){
                        $(v).remove();
                    }
                    
                });
                $("#modal-editar-pub").modal("hide");

            }
        });
    });

    $(document).on("click",".btn-pub-detail", (e) =>{
        console.log("Element ",$(e.target));
        let $idPost = $(e.target).attr("data-idpost");
        console.log("Id post ",$idPost)
        if(typeof $idPost == "undefined"){
            alert("Post no válido.");
            return;
        }
        if(typeof $user != "undefined"){
            $("#autorNewComment").text($user.nombre);
        }
        $("#modal-comentario-pub").attr("data-idpost",$idPost);
        $(".modal-body-comments").empty();
        let getCommentsPromise = new Promise( async (resolve, reject) => {
            $.ajax({
                method: "GET",
                url: "/comments/"+$idPost,
                dataType: "json",
                success: function($data) {
                    return $data;
                },
                error: function($data){
                    alert("No se pudieron recuperar los comentarios.");
                    reject(null);
                }
            })
            .done(function( data ) {
                resolve(data);
            });

        });
        getCommentsPromise.then((data) =>{
            console.log("Return promise comments ",data);
            if(data != null){
                $.each(data, function(i,v){
                    let comment = $(divComment);//Creamos el contenedor para el comentario
                    comment.find(".autorComment").text(v.nombre+" : "+moment(v.fechaRegistro).format("llll"));
                    comment.find(".commentPostText").text(v.comentario);
                    $(".modal-body-comments").append(comment);
                });
                $("#modal-comentario-pub").modal("show");
            }
        });
        
    });

    $("#modal-comentario-pub").on("hidden.bs.modal", () => {
        $("#autorNewComment").text("");
        $(".modal-body-comments").empty();
    });

    $("#btn-send-new-comment").on("click", () => {
        if(typeof $user == "undefined"){
            alert("El usuario no ha iniciado sesión.")
            return;
        }
        let $comentario = $("#newCommentPost"); 
        if($comentario.val().length < 4){
            alert("Escriba un comentario de al menos 5 caracteres.");
            return;
        }
        
        $idPost = $("#modal-comentario-pub").attr("data-idpost");
        if(typeof $idPost == "undefined"){
            alert("Post no válido.");
            return;
        }

        $comment = {
            "idUsuario":$user.idUsuario,
            "comentario": $comentario.val(),
            "idPost": $idPost
        };
        let saveCommentPromise = new Promise( async (resolve, reject) => {
            $.ajax({
                method: "POST",
                url: "/comments",
                dataType: "json",
                data: $comment,
                success: function($data) {
                    return $data;
                },
                error: function(qXHR, textStatus, errorThrown){
                    alert("Error al dar de alta "+textStatus);
                    return null;
                }
            })
            .done(function( data ) {
                if(data != null){
                    resolve(data);
                }
            }).fail((qXHR, textStatus, errorThrown) => {
                reject("Error "+textStatus);
            });
        });

        saveCommentPromise.then((data) =>{
            if(data != null){
                let comment = $(divComment);//Creamos el contenedor para el comentario
                comment.find(".autorComment").text($user.nombre+" : "+moment().format("llll"));
                comment.find(".commentPostText").text($comentario.val());
                $("#newCommentPost").val("");
                $(".modal-body-comments").prepend(comment);
            }
        });

    });
});