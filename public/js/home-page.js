if(typeof user != "undefined"){
    console.log("Entra aquí");
    
}else{
}

$(document).ready(($) => {
    moment.locale('es');
    let divPost = "<div class='col-4 div-post'>"+
                        "<div class='card' style='width: 18rem;'>"+
                        "<img src='' class='card-img-top'>"+
                        "<div class='card-body'>"+
                            "<h5 class='card-title'><span class='titulo'></span> <span class='autor'style='color: #C00'></span></h5>"+
                            "<p class='card-text'></p>"+
                            "<p class='card-text-date'></p>"+
                            "<button type='button' class='btn btn-danger btn-pub-detail'>Comentarios</button>"+
                            "<button type='button' class='btn btn-danger btn-pub-edit' style='margin-left: 10px;'>Editar</button>"+
                        "</div>"+
                    "</div>"+
                "</div>";

    $.ajax({
        method: "GET",
        url: "/posts",
        dataType: "json",
        success: function($data) {
            return $data;
        },
        error: function($data){
            alert("No se pudieron recuperar los posts.");
            return null;
        }
    })
    .done(function( data ) {
      //console.log( "Data: ", data );
        let $idUser = typeof $user != "undefined" ? $user.idUsuario : 0;
        console.log("Id usuario "+$idUser);
        if(data != null){
            $.each(data, function(i,v){
                let post = $(divPost);//Creamos el contenedor para el post
                post.find(".card-img-top").attr("src",v.media);
                post.find(".titulo").text(v.titulo+" por ");
                post.find('.autor').text(v.nombre);
                post.find(".card-text").text(v.descripcion);
                post.find(".btn-pub-detail").attr("data-idpost",v.idPost);
                post.find(".card-text-date").text(moment(v.fechaRegistro).format('llll'));
                post.find(".btn-pub-edit").attr("data-idpost",v.idPost);
                if($idUser != v.idUsuario) post.find(".btn-pub-edit").remove();
                $("#div-posts").append(post);                
            });
            
        }
    });

    $("#btn-login").on("click", () =>{
        let $usuario = $("#userLogin");
        let $pass = $("#passwordLogin");

        if($usuario.val().length <= 0){
            alert("Escriba su nombre de usuario");
            return;
        }

        if($pass.val().length <= 0){
            alert("Escriba su contraseña");
            return;
        }

        let $formLogin = {
            "usuario" : $usuario.val(),
            "password": $pass.val()
        }
        let getPostLoginPromise = new Promise( async (resolve, reject) => {
            $.ajax({
                method: "POST",
                url: "/login",
                dataType: "json",
                data: $formLogin,
                success: function($data) {
                    return $data;
                },
                error: function($dataFail){
                    return $dataFail;
                }
            })
            .done(function(data) {
                resolve(data);
            })
            .fail(function(data){
                if(data.status == "404"){
                    alert("Nombre de usuario y/o contraseña incorrectos.");
                }else{
                    alert("Ocurrió un error inesperado, intente en otro momento.");
                }
                reject(data);
            });

        });
        getPostLoginPromise.then((data) =>{
            console.log("Return promise comments ",data);
            if(data != null){
                window.localStorage.setItem('user',JSON.stringify(data));
                window.location.href = "/";
            }
        });
    });

    $("#btn-register").on("click", () => {
        let $usuario = $("#userRegister");
        let $pass = $("#passwordRegister");
        let $passRep = $("#passwordRegisterRep");

        if($usuario.val().length < 4){
            alert("El nombre de usuario debe ser de más de 4 caracteres.");
            return;
        }

        if($pass.val().length < 4){
            alert("La contraseña debe ser de más de 4 caracteres.");
            return;
        }

        if($passRep.val().length < 4){
            alert("La contraseña y su confirmación no coinciden.");
            return;
        }

        let $formRegister = {
            "nombre" : $usuario.val(),
            "password": $pass.val()
        }
        let getPostRegisterPromise = new Promise( async (resolve, reject) => {
            $.ajax({
                method: "POST",
                url: "/user",
                dataType: "json",
                data: $formRegister,
                success: function($data) {
                    return $data;
                },
                error: function($dataFail){
                    return $dataFail;
                }
            })
            .done(function(data) {
                resolve(data);
            })
            .fail(function(data){
                if(data.status == "400"){
                    alert("Existe una cuenta con ese nombre de usuario, elija otro por favor.");
                }else{
                    alert("Ocurrió un error inesperado, intente en otro momento.");
                }
                reject(data);
            });

        });
        getPostRegisterPromise.then((data) =>{
            console.log("Return promise comments ",data);
            if(data != null){
                window.localStorage.setItem('user',JSON.stringify(data));
                window.location.href = "/";
            }
        })
        
    });

});