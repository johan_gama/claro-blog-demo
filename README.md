# Claro Blog

Demo para Global Hitss de un CRUD básico a través de servicios web para la publicación de artículos usando el framework Mezzio/Laminas.

  - [PHP 7.4.13]
  - [Laminas]
  - [Mezzio 3]
  - MariaDB 10.4.17
  - [Composer]
  - [Boostrap 4.5]

### Ejecución

- La configuración de la base de datos se encuentra en: config\autoload\global.php y el dump de la base está en la raíz del proyecto claro_blog_demo.sql.
```php
return [
    'db' => [
        'driver' => 'Pdo',
        'dsn' => 'mysql:dbname=claro_blog_demo;host=localhost;user=root;password=;charset=utf8',
    ],
];
```

Instalar las dependencias vía Composer.

```sh
$ composer install
```

### Inyección de dependencias

```
Si sale una ventana para elegir dónde inyectar las dependencias. 
Elegir la opción 1 (config/config.php).
```

Para ejecutar el proyecto vía PHP CLI.

```sh
php -S 0.0.0.0:8080 -t public/
```
   [PHP 7.4.13]: https://www.apachefriends.org/download.html
   [Composer]: https://getcomposer.org/
   [Laminas]: https://getlaminas.org/
   [Mezzio 3]: https://docs.mezzio.dev/
   [Boostrap 4.5]: https://getbootstrap.com/docs/4.5/getting-started/introduction/