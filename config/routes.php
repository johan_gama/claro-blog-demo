<?php

declare(strict_types=1);

use Mezzio\Application;
use Mezzio\MiddlewareFactory;
use Psr\Container\ContainerInterface;
use Mezzio\Helper\BodyParams\BodyParamsMiddleware;

/**
 * laminas-router route configuration
 *
 * @see https://docs.laminas.dev/laminas-router/
 *
 * Setup routes with a single request method:
 *
 * $app->get('/', App\Handler\HomePageHandler::class, 'home');
 * $app->post('/album', App\Handler\AlbumCreateHandler::class, 'album.create');
 * $app->put('/album/:id', App\Handler\AlbumUpdateHandler::class, 'album.put');
 * $app->patch('/album/:id', App\Handler\AlbumUpdateHandler::class, 'album.patch');
 * $app->delete('/album/:id', App\Handler\AlbumDeleteHandler::class, 'album.delete');
 *
 * Or with multiple request methods:
 *
 * $app->route('/contact', App\Handler\ContactHandler::class, ['GET', 'POST', ...], 'contact');
 *
 * Or handling all request methods:
 *
 * $app->route('/contact', App\Handler\ContactHandler::class)->setName('contact');
 *
 * or:
 *
 * $app->route(
 *     '/contact',
 *     App\Handler\ContactHandler::class,
 *     Mezzio\Router\Route::HTTP_METHOD_ANY,
 *     'contact'
 * );
 */
return static function (Application $app, MiddlewareFactory $factory, ContainerInterface $container): void {

    //Principal
    $app->get('/', App\Handler\HomePageHandler::class, 'home');
    
    //Posts (publicaciones)
    $app->route('/posts', [BodyParamsMiddleware::class,App\Handler\PostHandler::class], ['GET', 'POST','PUT'], 'posts');
    $app->get('/posts/:idPost/:idUsuario', [BodyParamsMiddleware::class,App\Handler\PostHandler::class], 'posts.get');
    $app->delete('/posts/:idPost/:idUsuario', [BodyParamsMiddleware::class,App\Handler\PostHandler::class], 'posts.delete');

    //Comentarios de publicaciones
    $app->route('/comments', [BodyParamsMiddleware::class,App\Handler\ComentarioHandler::class], ['GET', 'POST'], 'comments.get.post');
    $app->route('/comments/:idPost', App\Handler\ComentarioHandler::class,['GET','DELETE'], 'comments.get.delete');
    
    //Usuarios
    $app->post('/user', [BodyParamsMiddleware::class,App\Handler\UsuarioHandler::class], 'user.post');
    $app->post('/login', [BodyParamsMiddleware::class,App\Handler\LoginHandler::class], 'usuario.post');
    
    //Miscellaneous
    $app->get('/api/ping', App\Handler\PingHandler::class, 'api.ping');
    $app->get('/article/:id', App\Handler\ArticuloPageHandler::class, 'article');
};
