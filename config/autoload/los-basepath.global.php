<?php
return [
    'los_basepath' => '/public/',
    'dependencies' => [
        'factories' => [
            LosMiddleware\BasePath\BasePathMiddleware::class => LosMiddleware\BasePath\BasePathMiddlewareFactory::class
        ],
    ],
];
