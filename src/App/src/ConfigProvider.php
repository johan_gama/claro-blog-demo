<?php

declare(strict_types=1);

namespace App;

use Laminas\ServiceManager\Factory\InvokableFactory;
use Mezzio\Helper\BodyParams\BodyParamsMiddleware;

/**
 * The configuration provider for the App module
 *
 * @see https://docs.laminas.dev/laminas-component-installer/
 */
class ConfigProvider
{
    /**
     * Returns the configuration array
     *
     * To add a bit of a structure, each section is defined in a separate
     * method which returns an array with its configuration.
     */
    public function __invoke(): array
    {
        return [
            'dependencies' => $this->getDependencies(),
            'templates'    => $this->getTemplates(),
        ];
    }

    /**
     * Returns the container dependencies
     */
    public function getDependencies(): array
    {
        return [
            'aliases' => [
                Model\UsuarioRepositoryInterface::class => Model\UsuarioRepository::class,
                Model\PostRepositoryInterface::class => Model\PostRepository::class,
                Model\ComentarioRepositoryInterface::class => Model\ComentarioRepository::class,                
            ],
            'invokables' => [
                Handler\PingHandler::class => Handler\PingHandler::class,
                Helper\BodyParams\BodyParamsMiddleware::class => Helper\BodyParams\BodyParamsMiddleware::class,
            ],
            'factories'  => [
                //Handlers
                Handler\HomePageHandler::class => Handler\HomePageHandlerFactory::class,
                Handler\UsuarioHandler::class => Handler\UsuarioHandlerFactory::class,
                Handler\ArticuloPageHandler::class => Handler\ArticuloPageHandlerFactory::class,
                Handler\PostHandler::class => Handler\PostHandlerFactory::class,
                Handler\ComentarioHandler::class => Handler\ComentarioHandlerFactory::class,
                Handler\LoginHandler::class => Handler\LoginHandlerFactory::class,
                //Repositories
                Model\UsuarioRepository::class => Factory\UsuarioRepositoryFactory::class,
                Model\PostRepository::class => Factory\PostRepositoryFactory::class,
                Model\ComentarioRepository::class => Factory\ComentarioRepositoryFactory::class,
            ],
        ];
    }

    /**
     * Returns the templates configuration
     */
    public function getTemplates(): array
    {
        return [
            'paths' => [
                'app'    => [__DIR__ . '/../templates/app'],
                'error'  => [__DIR__ . '/../templates/error'],
                'layout' => [__DIR__ . '/../templates/layout'],
            ],
        ];
    }
}
