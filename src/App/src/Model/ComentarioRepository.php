<?php

namespace App\Model;

use App\Model\Comentario;
use Laminas\Db\Sql\Sql;
use Laminas\Db\Adapter\AdapterInterface;
use Laminas\Db\Adapter\Driver\ResultInterface;
use Laminas\Db\ResultSet\ResultSet;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Hydrator\ReflectionHydrator;

class ComentarioRepository implements ComentarioRepositoryInterface{

    private $adapter;

    public function __construct(
        AdapterInterface $adapter
    ) {
        $this->adapter = $adapter;
    }

    public function save($comentario){
        $sql    = new Sql($this->adapter);
        $insert = $sql->insert();

        $insert->into('comentario');

        $insert->values([
                    'comentario' => $comentario['comentario'],
                    'idUsuario' => $comentario['idUsuario'],
                    'idPost' => $comentario['idPost'],
                ]);
        try{
            $statement = $sql->prepareStatementForSqlObject($insert);
            $results = $statement->execute();
            return $results->getGeneratedValue();
        }catch(Exception $e){
            throw new Exception('Error al dar de alta.');
        }
    }

    public function getById($idPost){
        $sql    = new Sql($this->adapter);
        $select = $sql->select();

        $select
            ->columns(['idPost','comentario','idComentario','fechaRegistro'])
            ->from('comentario');
        $select
        ->join(
            'usuario',        
            'usuario.idUsuario = comentario.idUsuario',
            ['nombre', 'idUsuario']
        );
        $select->where(['comentario.idPost = '.$idPost,'comentario.enable = 1','usuario.enable = 1'])->order('comentario.fechaRegistro DESC');

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $comentarios = [];
        if ($results instanceof ResultInterface && $results->isQueryResult()) {
            $resultSet = new HydratingResultSet(new ReflectionHydrator, new Comentario);
            $resultSet->initialize($results);

            foreach($resultSet as $comentario){
                $comentarios[] = $comentario;
            }
            return $comentarios;
        }

        return $comentarios;
    }

    public function getAll(){
        $sql    = new Sql($this->adapter);
        $select = $sql->select();

        $select
            ->columns(['idPost','comentario','idComentario','fechaRegistro'])
            ->from('comentario');
        $select
        ->join(
            'usuario',        
            'usuario.idUsuario = comentario.idUsuario',
            ['nombre', 'idUsuario']
        );
        $select->where(['comentario.enable = 1','usuario.enable = 1'])->order('comentario.fechaRegistro DESC');

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $comentarios = [];
        if ($results instanceof ResultInterface && $results->isQueryResult()) {
            $resultSet = new HydratingResultSet(new ReflectionHydrator, new Comentario);
            $resultSet->initialize($results);

            foreach($resultSet as $comentario){
                $comentarios[] = $comentario;
            }
            return $comentarios;
        }

        return $comentarios;
    }
}