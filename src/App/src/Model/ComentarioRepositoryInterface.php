<?php

namespace App\Model;

interface ComentarioRepositoryInterface{
    public function save($comentario);
    public function getById($idPost);
    public function getAll();
}