<?php
namespace App\Model;
use \JsonSerializable;
class Comentario implements JsonSerializable{

    protected $idComentario;
    protected $idPost;
    protected $comentario;
    protected $nombre;
    protected $idUsuario;
    protected $fechaRegistro;
    protected $enable;
    protected $fechaBaja;

    public function jsonSerialize() {
        return [
            'idComentario' => $this->idComentario,
            'idPost' => $this->idPost,
            'comentario' => $this->comentario,
            'nombre' => $this->nombre,
            'idUsuario' => $this->idUsuario,
            'fechaRegistro' => $this->fechaRegistro
        ];
    }

    public function getIdComentario(){
        return $this->idComentario;
    }

    public function getIdPost(){
        return $this->idPost;
    }

    public function getComentario(){
        return $this->comentario;
    }

    public function getNombre(){
        return $this->nombre;
    }

    public function getIdUsuario(){
        return $this->idUsuario;
    }

    public function getFechaRegistro(){
        return $this->fechaRegistro;
    }

    public function isEnable(){
        return $this->enable;
    }

    public function getFechaBaja(){
        return $this->fechaBaja;
    }

    public function setIdComentario($idComentario){
        $this->idComentario = $idComentario;
    }

    public function setIdPost($idPost){
        $this->idPost = $idPost;
    }

    public function setComentario($comentario){
        $this->comentario = $comentario;
    }

    public function setNombre($nombreUsuario){
        $this->nombre = $nombre;
    }

    public function setIdUsuario($idUsuario){
        $this->idUsuario = $idUsuario;
    }

    public function setFechaRegistro($fechaRegistro){
        $this->fechaRegistro = $fechaRegistro;
    }

    public function setEnable($enable){
        $this->enable = $enable;
    }

    public function setFechaBaja($fechaBaja){
        $this->fechaBaja = $fechaBaja;
    }

}