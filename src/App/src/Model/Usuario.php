<?php
namespace App\Model;
use \JsonSerializable;
class Usuario implements JsonSerializable {
    protected $idUsuario;
    protected $nombre;
    protected $fechaRegistro;
    protected $enable;
    protected $fechaBaja;

    public function jsonSerialize() {
        return [
            'idUsuario' => $this->idUsuario,
            'nombre' => $this->nombre
        ];
    }

    public function getIdUsuario(){
        return $this->idUsuario;
    }

    public function getNombre(){
        return $this->nombre;
    }

    public function getFechaRegistro(){
        return $this->fechaRegistro;
    }

    public function getEnable(){
        return $this->enable;
    }

    public function getFechaBaja(){
        return $this->fechaBaja;
    }

    public function setIdUsuario($idUsuario){
        $this->idUsuario = $idUsuario;
    }

    public function setNombre($nombre){
        $this->nombre = $nombre;
    }

    public function setFechaRegistro($fechaRegistro){
        $this->fechaRegistro = $fechaRegistro;
    }

    public function setEnable($enable){
        $this->enable = $enable;
    }

    public function setFechaBaja($fechaBaja){
        $this->fechaBaja = $fechaBaja;
    }

}