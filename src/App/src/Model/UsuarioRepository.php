<?php

namespace App\Model;

use App\Model\Usuario;
use Laminas\Db\Sql\Sql;
use Laminas\Db\Adapter\AdapterInterface;
use Laminas\Db\Adapter\Driver\ResultInterface;
use Laminas\Db\ResultSet\ResultSet;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Hydrator\ReflectionHydrator;
use \hash;

class UsuarioRepository implements UsuarioRepositoryInterface{

    private $adapter;

    public function __construct(
        AdapterInterface $adapter
    ) {
        $this->adapter = $adapter;
    }

    public function save($usuario){
        //La 'llave' para cifrar las contraseñas con SHA2('pass',512) es 1523525988381
        $sql    = new Sql($this->adapter);
        $insert = $sql->insert();

        $insert->into('usuario');

        $insert->values([
                    'nombre' => $usuario['nombre'],
                    'hash' => hash('sha512',$usuario['password']."1523525988381")//Usamos la llave
                ]);

        $statement = $sql->prepareStatementForSqlObject($insert);
        $results = $statement->execute();

        $res = $results->getGeneratedValue();
        if($res < 1){
            return null;
        }
        $user = new Usuario();
        $user->setIdUsuario($results->getGeneratedValue());
        $user->setNombre($usuario["nombre"]);
        return $user;
    }

    public function getById($idUsuario){
        return "Success";
    }

    public function getByName($nombre){
        $sql    = new Sql($this->adapter);
        $select = $sql->select();

        $select
            ->columns(["idUsuario","nombre"])
            ->from('usuario')
            ->where(["usuario.nombre = '".$nombre."'"])
            ->limit(1);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        
        $usuario = new Usuario();
        if ($results instanceof ResultInterface && $results->isQueryResult()) {
            $resultSet = new HydratingResultSet(new ReflectionHydrator, new Usuario);
            $resultSet->initialize($results);

            foreach($resultSet as $u){//Esto no es correcto, no me alcanza el tiempo para arreglarlo, lo siento.
                $usuario = $u;
            }
            if($usuario->getNombre() == null){
                return null;
            }
            return $usuario;
        }
        return null;
    }

    public function getAll(){
        $sql    = new Sql($this->adapter);
        $select = $sql->select();

        $select->from('usuario');

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute()->current();
        return $results;
    }

    public function getLogin($formLogin){
        $sql    = new Sql($this->adapter);
        $select = $sql->select();

        $select
            ->from('usuario')
            ->where(["usuario.nombre = '".$formLogin["usuario"]."'","usuario.hash = SHA2('".$formLogin["password"]."1523525988381',512)",'usuario.enable = 1'])
            ->limit(1);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        
        $usuario = new Usuario();
        if ($results instanceof ResultInterface && $results->isQueryResult()) {
            $resultSet = new HydratingResultSet(new ReflectionHydrator, new Usuario);
            $resultSet->initialize($results);

            foreach($resultSet as $u){//Esto no es correcto, no me alcanza el tiempo para arreglarlo, lo siento.
                $usuario = $u;
            }
            if($usuario->getNombre() == null){
                return null;
            }
            return $usuario;
        }
        return null;
    }
}