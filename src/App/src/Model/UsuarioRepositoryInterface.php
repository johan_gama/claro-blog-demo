<?php

namespace App\Model;

interface UsuarioRepositoryInterface{
    public function save($usuario);
    public function getById($idUsuario);
    public function getByName($nombre);
    public function getAll();
    public function getLogin($formLogin);
}