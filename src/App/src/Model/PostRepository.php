<?php

namespace App\Model;

use App\Model\Post;
use Laminas\Db\Sql\Sql;
use Laminas\Db\Adapter\AdapterInterface;
use Laminas\Db\Adapter\Driver\ResultInterface;
use Laminas\Db\ResultSet\ResultSet;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Hydrator\ReflectionHydrator;

class PostRepository implements PostRepositoryInterface{

    private $adapter;

    public function __construct(
        AdapterInterface $adapter
    ) {
        $this->adapter = $adapter;
    }

    public function save($post){
        $sql    = new Sql($this->adapter);
        $insert = $sql->insert();

        $insert->into('post');

        $insert->values([
                    'titulo' => $post['titulo'],
                    'descripcion' => $post['descripcion'],
                    'idUsuario' => $post['idUsuario'],
                    'media' => $post['media'],
                ]);

        $statement = $sql->prepareStatementForSqlObject($insert);
        $results = $statement->execute();
        return $results->getGeneratedValue();
    }

    public function getById($idPost,$idUsuario){
        $sql    = new Sql($this->adapter);
        $select = $sql->select();

        $select
            ->columns(['idPost','descripcion','titulo','media','fechaRegistro','idUsuario'])
            ->from('post')
            ->where(['post.idPost = '.$idPost,'post.idUsuario = '.$idUsuario,'post.enable = 1'])
            ->limit(1);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        
        $post = new Post();
        if ($results instanceof ResultInterface && $results->isQueryResult()) {
            $resultSet = new HydratingResultSet(new ReflectionHydrator, new Post);
            $resultSet->initialize($results);

            foreach($resultSet as $p){//Esto no es correcto, no me alcanza el tiempo para arreglarlo, lo siento.
                $post = $p;
            }

            return $post;
        }
        return null;
    }

    public function getAll(){
        $sql    = new Sql($this->adapter);
        $select = $sql->select();

        $select
            ->columns(['idPost','descripcion','titulo','media','fechaRegistro'])
            ->from('post');
        $select
        ->join(
            'usuario',        
            'usuario.idUsuario = post.idUsuario',
            ['nombre', 'idUsuario']
        );
        $select
            ->where(['post.enable = 1','usuario.enable = 1'])->order('post.fechaRegistro DESC');

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $posts = [];
        if ($results instanceof ResultInterface && $results->isQueryResult()) {
            $resultSet = new HydratingResultSet(new ReflectionHydrator, new Post);
            $resultSet->initialize($results);

            /*foreach ($resultSet as $row) {
                
                $post = new Post();
                $post->setIdUsuario($row->id_usuario);
                $post->setNombre($row->nombre);
                $intercepter = "Entra segundo IF ".$row->id_usuario." get ".$post->getIdUsuario()." nombre ".$row->nombre;
                array_push($posts,$post);
            }*/
            foreach($resultSet as $post){
                $posts[] = $post;
            }
            return $posts;
        }

        return $post;
    }

    public function update($post){
        $sql    = new Sql($this->adapter);
        $insert = $sql->update();

        $insert
            ->table('post')
            ->set(['titulo' => $post["titulo"],'descripcion' => $post["descripcion"], 'media' => $post["media"]]);

        $insert
            ->where(['post.idPost = '.$post["idPost"],'post.idUsuario = '.$post["idUsuario"]]);

        $statement = $sql->prepareStatementForSqlObject($insert);
        $results = $statement->execute();
        return $results->getAffectedRows();
    }

    public function delete($idPost,$idUsuario){
        $sql    = new Sql($this->adapter);
        $insert = $sql->update();

        $insert
            ->table('post')
            ->set(['enable' => '0']);

        $insert
            ->where(['post.idPost = '.$idPost,'post.idUsuario = '.$idUsuario]);

        $statement = $sql->prepareStatementForSqlObject($insert);
        $results = $statement->execute();
        return $results->getAffectedRows();
    }
}