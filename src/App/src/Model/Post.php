<?php
namespace App\Model;
use \JsonSerializable;
class Post implements JsonSerializable{

    protected $idPost;
    protected $titulo;
    protected $descripcion;
    protected $nombre;
    protected $idUsuario;
    protected $media;
    protected $fechaRegistro;
    protected $enable;
    protected $fechaBaja;

    public function jsonSerialize() {
        return [
            'idPost' => $this->idPost,
            'titulo' => $this->titulo,
            'descripcion' => $this->descripcion,
            'media' => $this->media,
            'nombre' => $this->nombre,
            'idUsuario' => $this->idUsuario,
            'fechaRegistro' => $this->fechaRegistro
        ];
    }

    public function getIdPost(){
        return $this->idPost;
    }

    public function getTitulo(){
        return $this->titulo;
    }

    public function getDescripcion(){
        return $this->descripcion;
    }

    public function getNombre(){
        return $this->nombre;
    }

    public function getIdUsuario(){
        return $this->idUsuario;
    }

    public function getMedia(){
        return $this->media;
    }

    public function getFechaRegistro(){
        return $this->fechaRegistro;
    }

    public function isEnable(){
        return $this->enable;
    }

    public function getFechaBaja(){
        return $this->fechaBaja;
    }

    public function setIdPost($idPost){
        $this->idPost = $idPost;
    }

    public function setTitulo($titulo){
        $this->titulo = $titulo;
    }

    public function setDescripcion($descripcion){
        $this->descripcion = $descripcion;
    }

    public function setNombre($nombreUsuario){
        $this->nombre = $nombre;
    }

    public function setIdUsuario($idUsuario){
        $this->idUsuario = $idUsuario;
    }

    public function setMedia($media){
        $this->media = $media;
    }

    public function setFechaRegistro($fechaRegistro){
        $this->fechaRegistro = $fechaRegistro;
    }

    public function setEnable($enable){
        $this->enable = $enable;
    }

    public function setFechaBaja($fechaBaja){
        $this->fechaBaja = $fechaBaja;
    }

}