<?php

namespace App\Model;

interface PostRepositoryInterface{
    public function save($post);
    public function getById($idPost, $idUsuario);
    public function getAll();
    public function update($post);
    public function delete($idPost,$idUsuario);
}