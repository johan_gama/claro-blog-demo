<?php

declare(strict_types=1);

namespace App\Handler;

use Mezzio\Router\RouterInterface;
use Mezzio\Template\TemplateRendererInterface;
use Psr\Container\ContainerInterface;
use Psr\Http\Server\RequestHandlerInterface;
use App\Model\ComentarioRepositoryInterface;
use Laminas\Db\Adapter\AdapterInterface;

use function get_class;

class ComentarioHandlerFactory
{
    public function __invoke(ContainerInterface $container): RequestHandlerInterface
    {
        $router   = $container->get(RouterInterface::class);
        $template = $container->has(TemplateRendererInterface::class)
            ? $container->get(TemplateRendererInterface::class)
            : null;
        $adapter = $container->get(AdapterInterface::class);

        return new ComentarioHandler(get_class($container), $router, $template, $adapter);
    }
}
