<?php

declare(strict_types=1);

namespace App\Handler;

use Laminas\Diactoros\Response\HtmlResponse;
use Laminas\Diactoros\Response\JsonResponse;
use Mezzio\LaminasView\LaminasViewRenderer;
use Mezzio\Plates\PlatesRenderer;
use Mezzio\Router;
use Mezzio\Template\TemplateRendererInterface;
use Mezzio\Twig\TwigRenderer;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Laminas\Db\Sql\Sql;
use App\Model\Comentario;
use App\Model\ComentarioRepository;
use Laminas\Db\Adapter\AdapterInterface;
use \Datetime;

class ComentarioHandler implements RequestHandlerInterface
{
    /** @var string */
    private $containerName;

    /** @var Router\RouterInterface */
    private $router;

    /** @var null|TemplateRendererInterface */
    private $template;

    private $adapter;

    private $comentarioRepository;

    public function __construct(
        string $containerName,
        Router\RouterInterface $router,
        ?TemplateRendererInterface $template = null,
        AdapterInterface  $adapter
    ) {
        $this->containerName = $containerName;
        $this->router        = $router;
        $this->template      = $template;
        $this->adapter = $adapter;
        $this->comentarioRepository = new ComentarioRepository($this->adapter);
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {   
        $data = [];
        $body = $request->getParsedBody();
        
        if($request->getMethod() == "POST"){//Alta comentario
            try{
                $idComentario = $this->comentarioRepository->save($body);
                if($idComentario != 0){
                    return new JsonResponse($idComentario,201);
                }else{
                    return new JsonResponse($idComentario,400);
                }
            }catch (Exception $e) {
                return new JsonResponse(["error" => "error".$e->getMessage()],400);
            }
        }else if($request->getMethod() == "GET"){
            $idPost = $request->getAttribute('idPost');
            if($idPost != null){
                return new JsonResponse($this->comentarioRepository->getById($idPost),200);
            }else{
                return new JsonResponse($this->comentarioRepository->getAll(),200);
            }
        }else if($request->getMethod() == "DELETE"){
            $idPost = $request->getAttribute('idPost');
            if($idPost == null){
                return new JsonResponse(["error" => "Sin id post a eliminar."],400);
            }
            return new JsonResponse(["error" => "Por implementar"],200);
            
        }
    }
}
