<?php

declare(strict_types=1);

namespace App\Handler;

use Laminas\Diactoros\Response\HtmlResponse;
use Laminas\Diactoros\Response\JsonResponse;
use Mezzio\LaminasView\LaminasViewRenderer;
use Mezzio\Plates\PlatesRenderer;
use Mezzio\Router;
use Mezzio\Template\TemplateRendererInterface;
use Mezzio\Twig\TwigRenderer;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Laminas\Db\Sql\Sql;
use App\Model\Post;
use App\Model\PostRepository;
use Laminas\Db\Adapter\AdapterInterface;
use \Datetime;

class PostHandler implements RequestHandlerInterface
{
    /** @var string */
    private $containerName;

    /** @var Router\RouterInterface */
    private $router;

    /** @var null|TemplateRendererInterface */
    private $template;

    private $adapter;

    private $postRepository;

    public function __construct(
        string $containerName,
        Router\RouterInterface $router,
        ?TemplateRendererInterface $template = null,
        AdapterInterface  $adapter
    ) {
        $this->containerName = $containerName;
        $this->router        = $router;
        $this->template      = $template;
        $this->adapter = $adapter;
        $this->postRepository = new PostRepository($this->adapter);
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {   
        $data = [];
        $body = $request->getParsedBody();
        if($request->getMethod() == "POST"){//Alta publicación
            return new JsonResponse($this->postRepository->save($body),201);
        }else if($request->getMethod() == "GET"){
            $idPost = $request->getAttribute('idPost');
            $idUsuario = $request->getAttribute('idUsuario');

            if($idPost == null || $idUsuario == null){
                return new JsonResponse($this->postRepository->getAll(),200);
            }else if($idPost != null && $idUsuario != null){
                $post = $this->postRepository->getById($idPost,$idUsuario);
                return new JsonResponse($post,200);
            }
            return new JsonResponse("Ruta no válida",400);
        }else if($request->getMethod() == "PUT"){
            $res = $this->postRepository->update($body);
            if($res != 0){
                return new JsonResponse($res,201);
            }
            return new JsonResponse("Actualización incorrecta.",400);
        }else if($request->getMethod() == "DELETE"){
            $idPost = $request->getAttribute('idPost');
            $idUsuario = $request->getAttribute('idUsuario');

            if($idPost == null || $idUsuario == null){
                return new JsonResponse(["error" => "Sin id post a eliminar."],400);
            }else if($idPost != null && $idUsuario != null){
                $res = $this->postRepository->delete($idPost,$idUsuario);
                if($res != 0){
                    return new JsonResponse(["message" => "Se eliminó correctamente."],200);
                }
            }
            return new JsonResponse(["error" => "Error al eliminar ".$res],400);
        }
    }
}
