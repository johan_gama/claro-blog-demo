<?php

declare(strict_types=1);

namespace App\Handler;

use Laminas\Diactoros\Response\HtmlResponse;
use Laminas\Diactoros\Response\JsonResponse;
use Mezzio\LaminasView\LaminasViewRenderer;
use Mezzio\Plates\PlatesRenderer;
use Mezzio\Router;
use Mezzio\Template\TemplateRendererInterface;
use Mezzio\Twig\TwigRenderer;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Laminas\Db\Sql\Sql;
use App\Model\Usuario;
use App\Model\UsuarioRepository;
use Laminas\Db\Adapter\AdapterInterface;
use \Datetime;

class LoginHandler implements RequestHandlerInterface
{
    /** @var string */
    private $containerName;

    /** @var Router\RouterInterface */
    private $router;

    /** @var null|TemplateRendererInterface */
    private $template;

    private $adapter;

    private $usuarioRepository;

    public function __construct(
        string $containerName,
        Router\RouterInterface $router,
        ?TemplateRendererInterface $template = null,
        AdapterInterface  $adapter
    ) {
        $this->containerName = $containerName;
        $this->router        = $router;
        $this->template      = $template;
        $this->adapter = $adapter;
        $this->usuarioRepository = new UsuarioRepository($this->adapter);
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {   
        $data = [];
        $body = $request->getParsedBody();
        
        if(($body["usuario"] == "" || $body["usuario"] == null) || ($body["password"] == "" || $body["password"] == "null")){
            return new JsonResponse(["error"=> "Datos de login inválidos"],400);
        }

        $user = $this->usuarioRepository->getLogin($body);
        if($user == null){
            return new JsonResponse(["error" => "Nombre de usuario y/o contraseña incorrectos."],404);
        }
        
        return new JsonResponse($user,200);
    }
}