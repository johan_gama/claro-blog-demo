<?php
namespace App\Factory;

use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use App\Model\ComentarioRepository;
use App\Model\ComentarioRepositoryInterface;
use Laminas\Db\Adapter\AdapterInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ComentarioRepositoryFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null){
        return new ComentarioRepository($container->get(ComentarioRepositoryInterface::class));
    }
}