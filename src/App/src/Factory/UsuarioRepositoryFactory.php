<?php
namespace App\Factory;

use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use App\Model\UsuarioRepository;
use App\Model\UsuarioRepositoryInterface;
use Laminas\Db\Adapter\AdapterInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

class UsuarioRepositoryFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null){
        return new UsuarioRepository($container->get(UsuarioRepositoryInterface::class));
    }
}